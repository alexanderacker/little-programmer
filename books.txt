In the series:
The Little Schemer 4th Edition
The Seasoned Schemer 2nd Edition
The Little MLer
The Reasoned Schemer
The Little Prover
The Reasoned Schemer, 2nd Edition
The Little Typer


Other related books:
Purely Functional Data Structures - Chris Okasaki
Types and Programming Languages - Benjamin C. Pierce
To Mock a Mockingbird and Other Logic Puzzels - Raymond M. Smullyan
Interactive Theorem Proving and Program Development: Coq Art: The Calculus of Inductive Constructions - Yves Bertot
Let Over Lambda - Doug Hoyte
The Book of Monads: Master the theory and practice of monads, applied to solve real world problems - Alejandro Serrano Mena
Thinking with Types. Type-Level Programming in Haskell - Sandy Maguire
