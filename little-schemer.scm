;; CHAPTER 1: Toys

(define atom?
  (lambda (x)
    (and (not (pair? x)) (not (null? x)))))

;; Car is only defined on non-empty lists. List -> Any (first list element)
;; Cdr is defined on non-empty lists. List -> List
;; Cons takes two arguments (Any, List) -> List
;; Null? is defined only for lists: Any -> Bool
;; Atom? takes a value, decides if atom: Any -> Bool
;; Eq? on non-numeric atoms: (Atom, Atom) -> Bool

;; CHAPTER 2: Do It, Do It Again, and Again, and Again

;; lat? List -> Bool
;; is the list a list of atoms?
(define lat?
  (lambda (l)
    (or (null? l)
        (and (atom? (car l)) (lat? (cdr l))))))

;; member? (Atom, List) -> Bool
;; does the list contain a given item?
(define member?
  (lambda (item l)
    (and (not (null? l))
         (or (eq? item (car l))
             (member? item (cdr l))))))

;; First Commandment: Always ask null? as the first cond in a list function

;; CHAPTER 3: Cons the Magnificent

;; rember (Atom, List) -> List
;; remove one of a given item from the list, no effect if item not in list
(define rember
  (lambda (item l)
    (cond
      ((null? l) '())
      ((eq? item (car l)) (cdr l))
      (else (cons (car l) (rember item (cdr l)))))))

;; Second Commandment: Use cons to build lists

;; firsts List of non-empty Lists -> List
;; makes a list containing the first element of each sublist
(define firsts
  (lambda (l)
    (cond
      ((null? l) '())
      (else (cons (caar l) (firsts (cdr l)))))))

;; Third Commandment: When list building, describe the first element, and then
;; cons it onto the natural recursion.

;; insertR (Atom, Atom, List) -> List
;; inserts the new atom to the right of the old atom once building a new list
(define insertR
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((eq? (car l) old) (cons (car l) (cons new (cdr l))))
      (else (cons (car l) (insertR new old (cdr l)))))))

;; insertL (Atom, Atom, List) -> List
;; inserts the new atom to the left of the old atom once building a new list
(define insertL
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((eq? (car l) old) (cons new l))
      (else (cons (car l) (insertL new old (cdr l)))))))

;; subst (Atom, Atom, List) -> List
;; substitutes the new atom for the old atom once building a new list
(define subst
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((eq? (car l) old) (cons new (cdr l)))
      (else (cons (car l) (subst new old (cdr l)))))))

;; subst2 (Atom, Atom, Atom, List) -> List
;; substitutes either the first occurrence of o1 or o2 by new building a new list
(define subst2
  (lambda (new o1 o2 l)
    (cond
      ((null? l) '())
      ((or (eq? (car l) o1) (eq? (car l) o2)) (cons new (cdr l)))
      (else (cons (car l) (subst2 new o1 o2 (cdr l)))))))

;; multirember (Atom, List) -> List
;; removes all occurrences of item in l
(define multirember
  (lambda (item l)
    (cond
      ((null? l) '())
      ((eq? item (car l)) (multirember item (cdr l)))
      (else (cons (car l) (multirember item (cdr l)))))))

;; multiinsertR (Atom, Atom, List) -> List
;; inserts the new atom to the right of the old atom in all occurrences, building a new list
(define multiinsertR
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((eq? (car l) old) (cons (car l) (cons new (multiinsertR new old (cdr l)))))
      (else (cons (car l) (multiinsertR new old (cdr l)))))))

;; multiinsertL (Atom, Atom, List) -> List
;; inserts the new atom to the left of the old atom in all occurrences, building a new list
(define multiinsertL
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((eq? (car l) old) (cons new (cons old (multiinsertL new old (cdr l)))))
      (else (cons (car l) (multiinsertL new old (cdr l)))))))

;; Fourth Commandment: Always change at least one argument while recurring.
;; It must be changed to be closer to termination.
;; The changing argument must be tested in a termination condition
;; For example Lists: cdr shortens the list and null? checks to terminate

;; multisubst (Atom, Atom, List) -> List
;; substitutes the new atom for the old atom in all places building a new list
(define multisubst
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((eq? (car l) old) (cons new (multisubst new old (cdr l))))
      (else (cons (car l) (multisubst new old (cdr l)))))))

;; CHAPTER 4: Numbers Games
;; NOTE: Functions prefixed with 'o' are custom implementations of scheme built-in functions!
(define zero 0)
;; add1 Nat -> Nat
(define add1
  (lambda (n)
    (+ n 1)))

;; sub1 Nat -> Nat
(define sub1
  (lambda (n)
    (- n 1)))

;; o+ (Nat, Nat) -> Nat
(define o+
  (lambda (n m)
    (cond
      ((zero? m) n)
      (else (add1 (o+ n (sub1 m)))))))

;; o- (Nat, Nat) -> Nat
(define o-
  (lambda (n m)
    (cond
      ((zero? m) n)
      (else (sub1 (o- n (sub1 m)))))))

;; addtup Tuple -> Nat
;; where tuple is a 'List of Nat'
(define addtup
  (lambda (t)
    (cond
      ((null? t) 0)
      (else (o+ (car t) (addtup (cdr t)))))))

;; First Commandment (first revision):
;; When recurring on a list of atoms ask 'null?'
;; When recurring on a number ask 'zero?'
;; then in both cases ask 'else'

;; Fourth Commandment (first revision):
;; Always change at least one argument while recurring to reach termination.
;; When using cdr, test termination with null?
;; When using sub1, test termination with zero?

;; o* (Nat, Nat) -> Nat
(define o*
  (lambda (n m)
    (cond
      ((zero? m) 0)
      (else (o+ n (o* n (sub1 m)))))))

;; Fifth Commandment: When building with +, use 0 for termination
;; When building with *, use 1 for the value of termination
;; When building with cons, use () for the value of termination

;; tup+ (Tuple, Tuple) -> Tuple
(define tup+
  (lambda (t1 t2)
    (cond
      ((null? t1) t2)
      ((null? t2) t1)
      (else (cons
             (o+ (car t1) (car t2))
             (tup+ (cdr t1) (cdr t2)))))))

;; o> (Nat, Nat) -> Bool
(define o>
  (lambda (n m)
    (or (and (zero? m) (not (zero? n)))
        (and
         (and (not (zero? n)) (not (zero? m)))
         (o> (sub1 n) (sub1 m))))))
;; as-previous just in conditional rather than boolean logic form
(define o>-normal
  (lambda (n m)
    (cond
      ((zero? n) #f)
      ((zero? m) #t)
      (else (o>-normal (sub1 n) (sub1 m))))))

;; o< (Nat, Nat) -> Bool
(define o<
  (lambda (n m)
    (or (and (zero? n) (not (zero? m)))
        (and
         (and (not (zero? n)) (not (zero? m)))
         (o< (sub1 n) (sub1 m))))))

;; o= (Nat, Nat) -> Bool
(define o=
  (lambda (n m)
    (or (and (zero? n) (zero? m))
        (and
         (and (not (zero? n)) (not (zero? m)))
         (o= (sub1 n) (sub1 m))))))
;; as-previous but built on top of <, >
(define o=-build
  (lambda (n m)
    (and (not (o> n m)) (not (o< n m)))))

;; oexpt (Nat, Nat) -> Nat
(define oexpt
  (lambda (b e)
    (cond
      ((zero? e) 1)
      (else (o* b (oexpt b (sub1 e)))))))

;; oquotient (Nat, Nat) -> Nat
;; Assumes that m is > 0
(define oquotient
  (lambda (n m)
    (cond
      ((< n m) 0)
      (else (add1 (oquotient (o- n m) m))))))

;; olength List -> Nat
(define olength
  (lambda (l)
    (cond
      ((null? l) 0)
      (else (add1 (olength (cdr l)))))))

;; pick (List, Nat) -> Any
;; picks the nth item of a list
;; Assumes the list is big enough
(define pick
  (lambda (n l)
    (cond
      ((zero? (sub1 n)) (car l))
      (else (pick (sub1 n) (cdr l))))))

;; rempick (List, Nat) -> Any
;; removes the nth item of a list
;; Assumes the list is big enough
(define rempick
  (lambda (n l)
    (cond
      ((zero? (sub1 n)) (cdr l))
      (else (cons (car l) (rempick (sub1 n) (cdr l)))))))

;; no-nums List -> List
;; removes all numbers in a list
(define no-nums
  (lambda (l)
    (cond
      ((null? l) '())
      ((number? (car l)) (no-nums (cdr l)))
      (else (cons (car l) (no-nums (cdr l)))))))

;; all-nums List -> Tuple
;; makes a tuple of all numbers in the list
(define all-nums
  (lambda (l)
    (cond
      ((null? l) '())
      ((number? (car l)) (cons (car l) (all-nums (cdr l))))
      (else (all-nums (cdr l))))))

;; eqan? (Atom Atom) -> Bool
;; Two atoms are the same type and are equal
(define eqan?
  (lambda (n m)
    (or (and (number? n) (number? m) (= n m))
        (and (not (and (number? n) (number? m))) (eq? n m)))))

;; occur (Atom List) -> Nat
;; Counts the number of times an atom occurs in a list
(define occur
  (lambda (item l)
    (cond
      ((null? l) 0)
      ((eq? item (car l)) (add1 (occur item (cdr l))))
      (else (occur item (cdr l))))))

;; one? Nat -> Bool
(define one?
  (lambda (n)
    (zero? (sub1 n))))

;; rempick-one (List, Nat) -> Any
;; rewritten, same function as previous 'rempick'
(define rempick-one
  (lambda (n l)
    (cond
      ((one? n) (cdr l))
      (else (cons (car l) (rempick (sub1 n) (cdr l)))))))

;; CHAPTER 5: *Oh My Gawd*: It's Full of Stars

;; rember* (Any, List) -> List
;; removes member within sublists as well
(define rember*
  (lambda (item l)
    (cond
      ((null? l) '())
      ((atom? (car l))
       (cond
         ((eq? item (car l)) (rember* item (cdr l)))
         (else (cons (car l) (rember* item (cdr l))))))
      (else (cons (rember* item (car l)) (rember* item (cdr l)))))))

;; insertR* (Atom, Atom, List) -> List
;; inserts the new atom to the right of the old atom in all places building a new list (enters sublists)
(define insertR*
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((atom? (car l))
       (cond
         ((eq? old (car l)) (cons (car l) (cons new (insertR* new old (cdr l)))))
         (else (cons (car l) (insertR* new old (cdr l))))))
      (else (cons (insertR* new old (car l)) (insertR* new old (cdr l)))))))

;; First Commandment (final version):
;; When recurring on a list of atoms ask 'null?'
;; When recurring on a number ask 'zero?'
;; When recurring on a list of S-expressions ask: 'null?' and 'atom? (car l)'
;; then in all cases ask 'else'

;; Fourth Commandment (final version):
;; Always change at least one argument while recurring to reach termination.
;;   For S-exprs use (car l) and (cdr l) if neither (null? l) nor (atom? (car l))
;; When using atoms: cdr, test termination with null?
;; When using nats: sub1, test termination with zero?

;; occur* (Atom, List) -> List
;; recursively counts occurrences of an atom in an s-expr
(define occur*
  (lambda (item l)
    (cond
      ((null? l) 0)
      ((atom? (car l))
       (cond
         ((eq? item (car l)) (add1 (occur* item (cdr l))))
         (else (occur* item (cdr l)))))
      (else (o+ (occur* item (car l)) (occur* item (cdr l)))))))
;; (occur* 'banana '((banana) (split ((((banana ice))) (cream (banana)) sherbet)) (banana) (bread) (banana brandy)))
;; subst* (Atom, Atom, List) -> List
;; recursively substitutes atoms new for old in an s-expr
(define subst*
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((atom? (car l))
       (cond
         ((eq? old (car l)) (cons new (subst* new old (cdr l))))
         (else (cons (car l) (subst* new old (cdr l))))))
      (else (cons (subst* new old (car l)) (subst* new old (cdr l)))))))

;; insertL* (Atom, Atom, List) -> List
;; inserts the new atom to the left of the old atom in all places building a new list (enters sublists)
(define insertL*
  (lambda (new old l)
    (cond
      ((null? l) '())
      ((atom? (car l))
       (cond
         ((eq? old (car l)) (cons new (cons (car l) (insertL* new old (cdr l)))))
         (else (cons (car l) (insertL* new old (cdr l))))))
      (else (cons (insertL* new old (car l)) (insertL* new old (cdr l)))))))

;; member* (Atom, List) -> List
;; checks if the atom is a member recursively in the s-expr
(define member*
  (lambda (item l)
    (cond
      ((null? l) #f)
      ((atom? (car l))
       (cond
         ((eq? item (car l)) #t)
         (else (member* item (cdr l)))))
      (else (or (member* item (car l)) (member* item (cdr l)))))))

;; leftmost List -> List
;; gets the leftmost atom if it exists, otherwise #f
(define leftmost
  (lambda (l)
    (cond
      ((null? l) #f)
      ((atom? (car l)) (car l))
      (else (leftmost (car l))))))

;; eqlist?-old (List, List) -> Bool
;; Either they are both null, both have the same atom and rest, both have the same s-expr and rest
(define eqlist?-old
  (lambda (l1 l2)
    (or (and (null? l1) (null? l2))
        (and (and (not (null? l1)) (not (null? l2)))
             (or (and (atom? (car l1)) (atom? (car l2)) (eqan? (car l1) (car l2)))
                 (and (not (atom? (car l1))) (not (atom? (car l2))) (eqlist?-old (car l1) (car l2))))
             (eqlist?-old (cdr l1) (cdr l2))))))

;; oequal? S-expr S-expr -> Bool
;; Checks if two s-exprs are equal
(define oequal?
  (lambda (s1 s2)
    (or (and (atom? s1) (atom? s2) (eqan? s1 s2))
        (and (not (atom? s1)) (not (atom? s2)) (eqlist? s1 s2)))))

;; eqlist? (List, List) -> Bool
;; The 'or' condition on atoms is replaced with the oequals? function
(define eqlist?
  (lambda (l1 l2)
    (or (and (null? l1) (null? l2))
        (and (and (not (null? l1)) (not (null? l2)))
             (oequal? (car l1) (car l2))
             (eqlist? (cdr l1) (cdr l2))))))

;; Sixth Commandment: Simplify only after your function is correct.

;; rember-s-expr (S-expr, List) -> List
;; removes all s-exprs matching the argument in the list
(define rember-s-expr
  (lambda (s l)
    (cond
      ((null? l) '())
      ((equal? s (car l)) (cdr l))
      (else (cons (car l) (rember-s-expr item (cdr l)))))))

;; CHAPTER 6: Shadows

;; numbered? Arithmetic Expr -> Bool
;; whether or not the s-expr is an arithmetic s-expr
(define numbered?-cond
  (lambda (a)
    (cond
      ((atom? a) (number? a))
      (else (and (numbered-cond? (car a)) (numbered-cond? (caddr a)))))))
;; defined with a boolean expression
(define numbered?
  (lambda (a)
    (or (and (atom? a) (number? a))
        (and (not (atom? a)) (numbered? (car a)) (numbered? (caddr a))))))

;; Seventh Commandment: Recur on subparts of the same nature.
;; Sublists of a list, sub s-exprs of an s-expr, etc...

;; value Numbered Arithmetic Expr -> Nat
;; operator goes in the middle
(define value-mid
  (lambda (n)
    (cond
      ((atom? n) n)
      ((eq? '+ (cadr n)) (o+ (value-mid (car n)) (value-mid (caddr n))))
      ((eq? '* (cadr n)) (o* (value-mid (car n)) (value-mid (caddr n))))
      (else (oexpt (value-mid (car n)) (value-mid (caddr n)))))))
;; operatpr goes on the left
(define value-first
  (lambda (n)
    (cond
      ((atom? n) n)
      ((eq? '+ (car n)) (o+ (value-first (cadr n)) (value-first (caddr n))))
      ((eq? '* (car n)) (o* (value-first (cadr n)) (value-first (caddr n))))
      (else (oexpt (value-first (cadr n)) (value-first (caddr n)))))))

(define 1st-expr (lambda (a) (cadr a)))
(define 2nd-expr (lambda (a) (caddr a)))
(define operator (lambda (a) (car a)))

(define value
  (lambda (n)
    (cond
      ((atom? n) n)
      ((eq? '+ (operator n)) (o+ (value (1st-expr n)) (value (2nd-expr n))))
      ((eq? '* (operator n)) (o* (value (1st-expr n)) (value (2nd-expr n))))
      (else (oexpt (value (1st-expr n)) (value (2nd-expr n)))))))

;; Eighth Commandment: Use helper functions to abstract from representations.

;; Secondary representation for natural numbers using lists
(define l-zero '())

(define l-zero?
  (lambda (n)
    (null? n)))

(define l-add1
  (lambda (n)
    (cons '() n)))

(define l-sub1
  (lambda (n)
    (cdr n)))

(define l-+
  (lambda (n m)
    (cond
      ((l-zero? n) m)
      (else (l-add1 (l-+ (l-sub1 n) m))))))

;; CHAPTER 7: Friends and Relations

;; set? List of Atoms -> Bool
;; verifies if a list is a set (no duplicates)
(define set?
  (lambda (l)
    (or (null? l)
        (and (not (member? (car l) (cdr l))) (set? (cdr l))))))

;; makeset List -> Set
;; deduplicates a list of atoms
(define makeset-naive
  (lambda (l)
    (cond
      ((null? l) '())
      ((member? (car l) (cdr l)) (makeset-naive (cdr l)))
      (else (cons (car l) (makeset-naive (cdr l)))))))
;; using multirember
(define makeset
  (lambda (l)
    (cond
      ((null? l) '())
      (else (cons (car l) (makeset (multirember (car l) (cdr l))))))))

;; subset? (Set, Set) -> Bool
;; whether the first set is a subset of the second
(define subset?
  (lambda (s1 s2)
    (or (null? s1)
        (and (member? (car s1) s2)
             (subset? (cdr s1) s2)))))

;; eqset? (Set, Set) -> Bool
;; whether the two sets are equal (remember ordering does not apply)
(define eqset?
  (lambda (s1 s2)
    (and (subset? s1 s2) (subset? s2 s1))))

;; insersect? (Set, Set) -> Bool
;; whether an element in set 1 is in s2
(define intersect?
  (lambda (s1 s2)
    (and (not (null? s1))
         (or (member? (car s1) s2)
             (intersect? (cdr s1) s2)))))

;; intersect (Set, Set) -> Set
;; take the intersection of two sets
(define intersect
  (lambda (s1 s2)
    (cond
      ((null? s1) '())
      ((member? (car s1) s2) (cons (car s1) (intersect (cdr s1) s2)))
      (else (intersect (cdr s1) s2)))))

;; union (Set, Set) -> Set
;; take the union of two sets
(define union
  (lambda (s1 s2)
    (cond
      ((null? s1) s2)
      ((member? (car s1) s2) (union (cdr s1) s2))
      (else (cons (car s1) (union (cdr s1) s2))))))

;; difference (Set, Set) -> Set
;; take the difference of two sets
(define difference
  (lambda (s1 s2)
    (cond
      ((null? s1) '())
      ((member? (car s1) s2) (difference (cdr s1) s2))
      (else (cons (car s1) (difference (cdr s1) s2))))))

;; intersectall List of Set -> Set
;; applies the intersection function over a list of sets
(define intersectall
  (lambda (l)
    (cond
      ((null? l) '())
      ((null? (cdr l)) (car l))
      (else (intersect (car l) (intersectall (cdr l)))))))

;; a-pair? Any
;; checks whether anything is a pair
(define a-pair?
  (lambda (p)
    (and (not (atom? p))
         (not (null? p))
         (not (null? (cdr p)))
         (null? (cddr p)))))

;; first Pair -> Any
(define first
  (lambda (p)
    (car p)))
;; second Pair -> Any
(define second
  (lambda (p)
    (cadr p)))
;; third Triple -> Any
(define third
  (lambda (t)
    (caddr t)))
;; build (Any, Any) -> Pair
(define build
  (lambda (f s)
    (cons f (cons s '()))))

;; fun? List of Pair -> Bool
;; checks if a list of pairs defining a relation is a set (a valid function)
(define fun?
  (lambda (l)
    (set? (firsts l))))

;; revrel-old List of Pair -> List of Pair
;; reverses the elements in each pair
(define revrel-old
  (lambda (l)
    (cond
      ((null? l) '())
      (else (cons (build (second (car l)) (first (car l))) (revrel-old (cdr l)))))))

;; revpair Pair -> Pair
;; reduces a pair
(define revpair
  (lambda (pair)
    (build (second pair) (first pair))))

;; revrel List of Pair -> List of Pair
;; reverses the elements in each pair
(define revrel
  (lambda (l)
    (cond
      ((null? l) '())
      (else (cons (revpair (car l)) (revrel (cdr l)))))))

;; one-to-one? List of Pair -> Bool
(define one-to-one?
  (lambda (f)
    (fun? (revrel f))))

;; CHAPTER 8: Lambda the Ultimate

;; rember-f (Comparison-fn, S-expr, List) -> List
;; removes an s-expr from an List
(define rember-f
  (lambda (c? item l)
    (cond
      ((null? l) '())
      ((c? item (car l)) (cdr l))
      (else (cons (car l) (rember-f c? item (cdr l)))))))

;; eq?-c Any -> (Any -> Bool)
;; curried eq?
(define eq?-c
  (lambda (e1)
    (lambda (e2)
      (eq? e1 e2))))

(define eq?-salad (eq?-c 'salad))

;; rember-fc Comparison-fn -> ((S-expr, List) -> List)
;; curried rember-f
(define rember-fc
  (lambda (c?)
    (lambda (item l)
      (cond
        ((null? l) '())
        ((c? item (car l)) (cdr l))
        (else (cons (car l) ((rember-fc c?) item (cdr l))))))))

(define rember-eq? (rember-fc eq?))

;; insertL-f Comparison-fn -> ((Atom, Atom, List) -> List)
;; curried version of insertL
(define insertL-f
  (lambda (c?)
    (lambda (new old l)
      (cond
        ((null? l) '())
        ((c? old (car l)) (cons new (cons old (cdr l))))
        (else (cons (car l) ((insertL-f c?) new old (cdr l))))))))

;; insertR-f Comparison-fn -> ((Atom, Atom, List) -> List)
;; curried version of insertR
(define insertR-f
  (lambda (c?)
    (lambda (new old l)
      (cond
        ((null? l) '())
        ((c? old (car l)) (cons old (cons new (cdr l))))
        (else (cons (car l) ((insertR-f c?) new old (cdr l))))))))

;; seqL (Any, Any, List) -> List
;; Returns list of form new::old::l
(define seqL
  (lambda (new old l)
    (cons new (cons old l))))

;; seqR (Any, Any, List) -> List
;; Returns list of form old::new::l
(define seqR
  (lambda (new old l)
    (cons old (cons new l))))

;; insert-g (Any, Any, List) -> ((Atom, Atom, List)->List)
;; curried and generalized version of insert
(define insert-g
  (lambda (s)
    (lambda (new old l)
      (cond
        ((null? l) '())
        ((eq? old (car l)) (s new old (cdr l)))
        (else (cons (car l) ((insert-g s) new old (cdr l))))))))

;; various ways to redefine insertL/R
(define insertL-new (insert-g seqL))
(define insertR-new (insert-g seqR))
(define insertL-new2
  (insert-g
   (lambda (new old l)
     (cons new (cons old l)))))

;; redefine substitute to work with insert-g
(define seqS
  (lambda (new old l)
    (cons new l)))
(define subst-new (insert-g seqS))

;; redefine rember to work with insert-g
(define seqR
  (lambda (new old l)
    l))
(define rember-new
  (lambda (item l)
    ((insert-g seqR) #f item l)))

;; Ninth Commandment: Abstract common patterns with a new function.

;; atom-to-function ('+ | '* | '^) -> (o+ | o* | o^)
;; maps operator atom to respective operator function
(define atom-to-function
  (lambda (a)
    (cond
      ((eq? '+ a) o+)
      ((eq? '* a) o*)
      (else o^))))

;; redefine value to work with the atom-to-function function
(define value-new
  (lambda (n)
    (cond
      ((atom? n) n)
      (else ((atom-to-function (operator n))
             (value (1st-expr n))
             (value (2nd-expr n)))))))

;; multirember-f Comparison-fn -> ((Atom, List) -> List)
;; removes all occurrences of item in l, currying for comparison-fn
(define multirember-f
  (lambda (c?)
    (lambda (item l)
      (cond
        ((null? l) '())
        ((c? item (car l)) ((multirember-f c?) item (cdr l)))
        (else (cons (car l) ((multirember-f c?) item (cdr l))))))))

(define multirember-eq? (multirember-f eq?))
(define eq?-tuna (eq?-c 'tuna))

;; multiremberT (Predicate-fn, List) -> List
;; multirember except the condition is a predicate function now
(define multiremberT
  (lambda (p? l)
    (cond
      ((null? l) '())
      ((p? (car l)) (multiremberT p? (cdr l)))
      (else (cons (car l) (multiremberT p? (cdr l)))))))

;; multirember&co (Atom, List, Continuation) -> Any
;; analysis follows
(define multirember&co
  (lambda (item l col)
    (cond
      ((null? l) (col '() '()))
      ((eq? item (car l)) (multirember&co item (cdr l)
                                          (lambda (newl seen)
                                            (col newl (cons (car l) seen)))))
      (else (multirember&co item (cdr l)
                            (lambda (newl seen)
                              (col (cons (car l) newl) seen)))))))
;; Analysis:
;; if eq? continuation becomes (λ (newl, seen) (col newl (car l)::seen))
;; if not eq? continuation is  (λ (newl, seen) (col (car l)::newl seen))
;; So... if equal we add head to seen list
;; If not equal we add head to the 'new list'
;; then in the end we run this col function on two empty lists

;; a-friend (List, List) -> Bool
;; this is our continuation, continuation analysis below
(define a-friend
  (lambda (x y)
    (null? y)))
;; Analysis:
;; since by convention when we see something it goes into the list 'y'
;; this function tells us if we've seen nothing when used as a continuation

;; new-friend (List, List) -> Continuation
;; (multirember&co 'tuna '(tuna) a-friend)
(define new-friend
  (lambda (newl seen)
    (a-friend newl
              (cons 'tuna seen))))

;; latest-friend (List, List) -> Continuation
;; (multirember&co 'tuna '(and tuna) a-friend)
(define latest-friend
  (lambda (newl seen)
    (a-friend (cons 'and newl) seen)))

;; Tenth Commandment: Build functions to collect more than one value at once.

;; multiinsertLR (Atom, Atom, Atom, List) -> List
;; inserts to the left of oldL, right of oldR, oldL != oldR
(define multiinsertLR
  (lambda (new oldL oldR l)
    (cond
      ((null? l) '())
      ((eq? (car l) oldL) (cons new (cons (car l) (multiinsertLR new oldL oldR (cdr l)))))
      ((eq? (car l) oldR) (cons (car l) (cons new (multiinsertLR new oldL oldR (cdr l)))))
      (else (cons (car l) (multiinsertLR new oldL oldR (cdr l)))))))

;; multiinsertLR&co (Atom, Atom, Atom, List, (List, Nat, Nat -> T)) -> T
;; inserts to the left of oldL, right of oldR, oldL != oldR
;; uses a continuation
(define multiinsertLR&co
  (lambda (new oldL oldR l col)
    (cond
      ((null? l) (col '() 0 0))
      ((eq? (car l) oldL) (multiinsertLR&co new oldL oldR (cdr l)
                                            (lambda (newl lc rc)
                                              (col (cons new (cons (car l) newl)) (add1 lc) rc))))
      ((eq? (car l) oldR) (multiinsertLR&co new oldL oldR (cdr l)
                                            (lambda (newl lc rc)
                                              (col (cons (car l) (cons new newl)) lc (add1 rc)))))
      (else (multiinsertLR&co new oldL oldR (cdr l)
                              (lambda (newl lc rc)
                                (col (cons (car l) newl) lc rc)))))))

;; evens-only* List->List
;; removes all odd numbers from a list of nested lists
(define evens-only*
  (lambda (l)
    (cond
      ((null? l) '())
      ((atom? (car l))
       (cond
         ((even? (car l)) (cons (car l) (evens-only* (cdr l))))
         (else (evens-only* (cdr l)))))
      (else (cons (evens-only* (car l)) (evens-only* (cdr l)))))))

;; evens-only*&co (List ((List Nat Nat)->T))->T
;; take sum of all odd numbers and product of evens given to continuation
(define evens-only*&co
  (lambda (l col)
    (cond
      ((null? l) (col '() 1 0))
      ((atom? (car l))
       (cond
         ((even? (car l)) (evens-only*&co (cdr l)
                                          (lambda (newl n m)
                                            (col (cons (car l) newl)
                                                 (o* (car l) n) m))))
         (else (evens-only*&co (cdr l)
                               (lambda (newl n m)
                                 (col newl
                                      n (o+ (car l) m)))))))
      (else (evens-only*&co (car l)
                            (lambda (l1 n1 m1)
                              (evens-only*&co (cdr l)
                                              (lambda (newl n m)
                                                (col (cons l1 newl)
                                                     (o* n1 n)
                                                     (o+ m1 m))))))))))

;; the-last-friend (List, Nat, Nat) -> List
;; This is our continuation used to check evens-only*&co
(define the-last-friend
  (lambda (newl product sum)
    (cons sum (cons product newl))))

;; CHAPTER 9: ...and Again, and Again, and Again,...

;; looking (Atom, List) -> Bool
;; keep-looking based on the value of the car
(define looking
  (lambda (item l)
    (keep-looking item (pick 1 l) l)))

;; keep-looking (Atom, Atom, List) -> Bool
;; If the second atom is a number, keep looking with the value of the list at the number
;; if it is an atom, return whether or not it is the same as item.
(define keep-looking
  (lambda (item val l)
    (cond
      ((number? val) (keep-looking item (pick val l) l))
      (else (eq? item val)))))

;; eternity Untypeable
;; a short function which never reaches it's end.
(define eternity
  (lambda (x)
    (eternity x)))

;; shift Pair(Pair, Pair) -> Pair(Atom, Pair(Atom, Pair))
;; shifts the second part of first pair into second component
(define shift
  (lambda (p)
    (build (first (first pair))
           (build (second (first pair))
                  (second pair)))))

;; align Pair(T, T) -> Pair
;; Shifts first part of pair until we've only got an atom left
(define align
  (lambda (p)
    (cond
      ((atom? p) p)
      ((a-pair? (first p)) (align (shift p)))
      (else (build (first p) (align (second p)))))))

;; length* Pair -> Nat
;; Counts the number of atoms in a pair
(define length*
  (lambda (p)
    (cond
      ((atom? p) 1)
      (else (o+ (length* (first p)) (length* (second p)))))))

;; weight* Pair -> Nat
;; A better way to check the pair size
(define weight*
  (lambda (p)
    (cond
      ((atom? p) 1)
      (else (o+ (o* (weight* (first p)) 2)
                (weight* (second p)))))))

;; shuffle Pair -> Nonterminating
;; Like align but instead of shift it repeatedly reverses the pair...
(define shuffle
  (lambda (p)
    (cond
      ((atom? p) p)
      ((a-pair? (first p)) (shuffle (revpair p)))
      (else (build (first p) (shuffle (second p)))))))

;; C Nat -> ??? (Either Nat or Nonterminating)
;; Collatz function...
(define C
  (lambda (n)
    (cond
      ((one? n) 1)
      ((even? n) (C (oquotient n 2)))
      (else (C (add1 (o* 3 n)))))))

;; A Pair(Nat, Nat) -> Nat
;; Ackermann function...
(define A
  (lambda (n m)
    (cond
      ((zero? n) (add1 m))
      ((zero? m) (A (sub1 n) 1))
      (else (A (sub1 n) (A n (sub1 m)))))))

;; Notice how the arguments of the Ackermann function do not
;; trivially decrease the recursion in some way, like align or shuffle


;; will-stop? (List->Any) -> Bool
;; returns whether the given function stops given the empty list
;; (define will-stop?
;; ...)

;; last-try Untypeable
;; a tricky function that never stops
(define last-try
  (lambda (x)
    (and (will-stop? last-try)
         (eternity x))))
;; (will-stop? last-try) -> (will-stop? last-try) + (eternity x) ...
;; notice how we can never resolve will-stop?
;; so that means it won't stop

;; length_0 List -> 0 or Untypeable
(define length_0
  (lambda (l)
    (cond
      ((null? l) 0)
      (else (add1 (eternity (cdr l)))))))

(define length<=1
  (lambda (l)
    (cond
      ((null? l) 0)
      (else (add1 (length_0 (cdr l)))))))

(define length<=2
  (lambda (l)
    (cond
      ((null? l) 0)
      (else (add1 (length_1 (cdr l)))))))

;; So we have an infinite number of length functions, 1 for each length

;; let's factor out the length function as f
(define factorout
  (lambda (f)
    (lambda (l)
      (cond
        ((null? l) 0)
        (else (add1 (f (cdr l)))))) eternity))

;; let's try a mk-length function that makes the next length function
;; ((lambda (mk-length)
;;    (mk-length eternity))
;;  (lambda (length)
;;    (lambda (l)
;;      (cond
;;        ((null? l) 0)
;;        (else (add1 (length (cdr l))))))))
;; length one
;; ((lambda (mk-length) (mk-length (mk-length eternity))) ...)

;; length two
;;((lambda (mk-length) (mk-length (mk-length (mk-length eternity)))) ...)

(define length??-any
  ((lambda (mk-length) (mk-length mk-length))
   (lambda (mk-length)
     (lambda (l)
       (cond
         ((null? l) 0)
         (else (add1 ((mk-length mk-length) (cdr l)))))))))

(define length-any
  ((lambda (mk-length) (mk-length mk-length))
   (lambda (mk-length)
     ((lambda (length)
        (lambda (l)
          (cond
            ((null? l) 0)
            (else (addl (length (cdr l)))))))
      (lambda (x) ((mk-length mk-length) x))))))

;; The Y Combinator
(define Y
  (lambda (le)
    ((lambda (f) (f f))
     (lambda (f)
       (le (lambda (x) ((f f) x)))))))

;; CHAPTER 10: What is the Value of All of This?

;; new-entry (Set, List) -> Pair(Set, List)
;; where |Set| = Length of list
(define new-entry build)

;; lookup-in-entry (Atom, Entry, (Atom->T)) ->  T or vals[key]=Any
;; uses 'entry' like a key,value list and looks up name
;; if it works, returns the value otherwise calls 'entry-f'
(define lookup-in-entry
  (lambda (name entry entry-f)
    (lookup-in-entry-help name
                          (first entry)
                          (second entry)
                          entry-f)))

;; lookup-in-entry-help (Atom, List, List, (Atom->T)) -> T or vals[key]=Any
;; the same as lookup-in-entry except the Entry is split into two lists
;; so that we can do recursion on them at the same time
(define lookup-in-entry-help
  (lambda (name keys vals entry-f)
    (cond
      ((null? keys) (entry-f name))
      ((eq? name (car keys)) (car vals))
      (else (lookup-in-entry-help name
                                  (cdr keys)
                                  (cdr vals)
                                  entry-f)))))

;; extend-table (Entry, Table) -> Table
(define extend-table cons)

;; lookup-in-table (Atom, Table, (Atom->T)) -> T or value of name in 1st matched key
(define lookup-in-table
  (lambda (name table table-f)
    (cond
      ((null? table) (table-f name))
      (else (lookup-in-entry name
                        (car table)
                        (lambda (name)
                          (lookup-in-table name
                                           (cdr table)
                                           table-f)))))))

;; expression-to-action Expr -> Action
(define expression-to-action
  (lambda (e)
    (cond
      ((atom? e) (atom-to-action e))
      (else (list-to-action e)))))

;; atom-to-action Atom -> Action
(define atom-to-action
  (lambda (e)
    (cond
      ((number? e) *const)
      ((eq? e #t) *const)
      ((eq? e #f) *const)
      ((eq? e 'cons) *const)
      ((eq? e 'car) *const)
      ((eq? e 'cdr) *const)
      ((eq? e 'null?) *const)
      ((eq? e 'eq) *const)
      ((eq? e 'atom?) *const)
      ((eq? e 'zero?) *const)
      ((eq? e 'add1) *const)
      ((eq? e 'sub1) *const)
      ((eq? e 'number?) *const)
      (else *identifier))))

;; list-to-action List -> Action
(define list-to-action
  (lambda (e)
    (cond
      ((atom? (car e))
       (cond
         ((eq? (car e) 'quote) *quote)
         ((eq? (car e) 'lambda) *lambda)
         ((eq? (car e) 'cond) *cond)
         (else *application)))
       (else *application))))

;; value Expression -> Value
(define value
  (lambda (e)
    (meaning e '())))

;; meaning (Expression, Table) -> Value
(define meaning
  (lambda (e table)
    ((expression-to-action e) e table)))

;; *const Gets a constant
(define *const
  (lambda (e table)
    (cond
      ((number? e) e)
      ((eq? e #t) #t)
      ((eq? e #f) #f)
      (else (build 'primitive e)))))

;; *quote Creates a quote
(define *quote
  (lambda (e table)
    (text-of e)))

;; text-of just an alias
(define text-of second)

;; *identifier Gets an identifier
(define *identifier
  (lambda (e table)
    (lookup-in-table e table initial-table)))

;; initial-table The empty table
(define initial-table
  (lambda (name)
    (car '())))

;; *lambda returns ('non-primitive (table, formals, body))
(define *lambda
  (lambda (e table)
    (build 'non-primitive (cons table (cdr e)))))

;; *lambda helpers
(define table-of first)
(define formals-of second)
(define body-of third)

;; *cond helpers
(define question-of first)
(define answer-of second)
(define cond-lines-of cdr)

;; evcond *cond helper function
(define evcon
  (lambda (lines table)
    (cond
      ((else? (question-of (car lines)))
       (meaning (answer-of (car lines)) table))
      ((meaning (question-of (car lines)) table)
       (meaning (answer-of (car lines)) table))
      (else (evcon (cdr lines) table)))))

;; else? checks if anything is 'else
(define else?
  (lambda (x)
    (cond
      ((atom? x) (eq? x 'else))
      (else #f))))

;; *cond
(define *cond
  (lambda (e table)
    (evcon (cond-lines-of e) table)))

;; evlis Takes a list of arguments and gets their meanings
(define evlis
  (lambda (args table)
    (cond
      ((null? args) '())
      (else (cons (meaning (car args) table)
                  (evlis (cdr args) table))))))

;; *application helpers
(define function-of car)
(define arguments-of cdr)

;; *application
(define *application
  (lambda (e table)
    (oapply
     (meaning (function-of e) table)
     (evlis (arguments-of e) table))))

;; apply helpers
(define primitive?
  (lambda (l)
    (eq? (first l) 'primitive)))
(define non-primitive?
  (lambda (l)
    (eq? (first l) 'non-primitive)))

;; oapply
(define oapply
  (lambda (fun vals)
    (cond
      ((primitive? fun) (apply-primitive (second fun) vals))
      ((non-primitive? fun) (apply-closure (second fun) vals)))))

;; apply-primitive
(define apply-primitive
  (lambda (name vals)
    (cond
      ((eq? name 'cons) (cons (first vals) (second vals)))
      ((eq? name 'car) (car (first vals)))
      ((eq? name 'cdr) (cdr (first vals)))
      ((eq? name 'null?) (null? (first vals)))
      ((eq? name 'eq?) (eq? (first vals) (second vals)))
      ((eq? name 'atom?) (oatom? (first vals)))
      ((eq? name 'zero?) (zero? (first vals)))
      ((eq? name 'add1) (add1 (first vals)))
      ((eq? name 'sub1) (sub1 (first vals)))
      ((eq? name 'number?) (number? (first vals))))))

;; oatom? Our special version of atom?
(define oatom?
  (lambda (x)
    (cond
      ((atom? x) #t)
      ((null? x) #f)
      ((primitive? x) #t)
      ((non-primitive? x) #t)
      (else #f))))

;; apply-closure
(define apply-closure
  (lambda (closure vals)
    (meaning (body-of closure)
             (extend-table (new-entry (formals-of closure)
                                      vals)
                           (table-of closure)))))
