;; LOAD Kanren implementation
(load "CodeFromTheReasonedSchemer2ndEd/trs2-impl.scm")
;; Missing definitions assumed by trs2-impl but not in Scheme
(define add1
  (lambda (n)
    (+ n 1)))

(define sub1
  (lambda (n)
    (- n 1)))

;; CHAPTER 1: Playthings

;; Goal: something that suceeds, fails or has no value
;;       #s is a goal that succeeds
;;       #u is a goal that fails

;; (run* q fail) -> ()
;; (run* q succeed) -> (_0)
;; (run* q (== q 'answer)) -> (answer)

;; First Law of ==: (== v w) is equivalent to (== w v)

;; Variable Freshness:
;;   Every variable is initially fresh
;;   It is no longer fresh when either:
;;   1) Associated with a non-variable value
;;   2) Associated with a variable, which itself is not fresh

;; Commas in expressions only precede variables
;; Non-variables get quoted by it

;; Fuse two variables
;;   (run* q (fresh (x) (== x q)))

;; Two variables are different if they have not been fused!

;; A variable u OCCURSs in a variable v when u
;; (or anything fused to u) appears in v's value

;; Second Law of ==: if fresh(x) then (== v x) succeeds and associates v with x
;;                   unless x occurs in v.

;; conj2 is the logical and of two expressions
;; disj2 is the logical or of two expressions

;; Each value produced by a run* expression is reified independently.
;; This means the numbering of reified variables always restarts from 0 per val

(defrel (teacupo t)
  (disj2 (== 'tea t) (== 'cup t)))

;; conde is the disjunction of conjunctions

;; Law of conde: Every successful conde line contributes one or more values.

;; CHAPTER 2: Teaching Old Toys New Tricks
(defrel (caro p a)
  (fresh (d) (== (cons a d) p)))

(defrel (cdro p d)
  (fresh (a) (== (cons a d) p)))

(defrel (conso-old a d p)
  (car p a)
  (cdr p d))

(defrel (conso a d p)
  (== `(,a . ,d) p))

(defrel (nullo l)
  (== '() l))

(defrel (pairo p)
  (fresh (a d)
         (conso a d p)))

;; singleton? List -> Bool
;; checks if a list has only one element
(define (singleton? l)
  (cond
    ((pair? l) (null? (cdr l)))
    (else #f)))

;; my implementation of singletono
(defrel (singletono-my l)
  (conde
   ((pairo l) (fresh (a) (conso a '() l)))
   (fail)))

(defrel (singletono-first l)
  (conde
   ((pairo l) (fresh (d)
                     (cdro l d)
                     (nullo d)))
   (fail)))

;; The Translation (Initial):
;; To translate function to relation
;;   1) Replace define with defrel
;;   2) Unnest each expression in each cond line
;;   3) Replace cond with conde
;;   4) To unnest #t replace with succeed
;;   5) To unnest #f replace with fail

(defrel (singletono-second l)
  (conde
   ((pairo l) (fresh (d) (cdro l d) (nullo d)))))

;; Law of fail: Any conde with fail as a top-level goal can't contribute values

(defrel (singletono-cons l)
  (fresh (d)
         (cdro l d)
         (nullo d)))

;; Define caro and cdro in terms of conso
(defrel (caro-cons p a)
  (fresh (d) (conso a d p)))

(defrel (cdro-cons p d)
  (fresh (a) (conso a d p)))

;; CHAPTER 3: Seeing Old Friends in New Ways
(defrel (listo-first l)
  (conde
   ((nullo l) succeed)
   ((pairo l)
    (fresh (d)
           (cdro l d)
           (listo-first d)))
   (succeed fail)))

(defrel (listo-second l)
  (conde
   ((nullo l) succeed)
   ((fresh (d)
           (cdro l d)
           (listo-second d)))))

(defrel (listo l)
  (conde
   ((nullo l))
   ((fresh (d)
           (cdro l d)
           (listo d)))))

;; Law of succeed: Any top-level succeed can be removed from a conde line

;; lol? List -> Bool
;; checks if a list is a list of lists
(define (lol? l)
  (cond
    ((null? l) #t)
    ((list? (car l)) (lol? (cdr l)))
    (#t #f)))

(defrel (lolo l)
  (conde
   ((nullo l))
   ((fresh (a)
           (caro l a)
           (listo a))
    (fresh (d)
           (cdro l d)
           (lolo d)))))

(defrel (singletono l)
  (fresh (a)
         (== `(,a) l)))

(defrel (loso l)
  (conde
   ((nullo l))
   ((fresh (a)
           (caro l a)
           (singletono a))
    (fresh (d)
           (cdro l d)
           (loso d)))))

;; member? (Atom, List) -> Bool
;; does the list contain a given item?
(define (member? x l)
  (cond
    ((null? l) #f)
    ((equal? (car l) x) #t)
    (#t (member? x (cdr l)))))

(defrel (membero-first x l)
  (conde
   ((nullo l) fail)
   ((fresh (a)
           (car l a)
           (== a x))
    succeed)
   (succeed (fresh (d)
                   (cdr l d)
                   (membero-first x d)))))

(defrel (membero-second x l)
  (conde
   ((fresh (a)
           (car l a)
           (== a x))
    succeed)
   ((fresh (d)
           (cdr l d)
           (membero-second x d)))))

(defrel (membero x l)
  (conde
   ((caro l x))
   ((fresh (d)
           (cdro l d)
           (membero x d)))))

(defrel (proper-membero x l)
  (conde
   ((caro l x)
    (fresh (d)
           (cdro l d)
           (listo d)))
   ((fresh (d)
           (cdro l d)
           (proper-membero x d)))))

;; proper-member? (Atom, List) -> Bool
;; does the list contain a given item?
;; * enforces l is a list
(define (proper-member? x l)
  (cond
    ((null? l) #f)
    ((equal? (car l) x) (list? (cdr l)))
    (#t (proper-member? x (cdr l)))))

;; CHAPTER 4: Double Your Fun

;; append (List, List) -> List
;; creates one list which is the first list with the second list after
;; doesn't care if t is actually a list, but our relation needs to care
(define (append-my l t)
  (cond
    ((null? l) t)
    (#t (cons (car l) (append-my (cdr l) t)))))

(defrel (appendo-first l t out)
  (conde
   ((nullo l) (== t out))
   ((fresh (res)
           (fresh (d)
                  (cdro l d)
                  (appendo-first d t res))
           (fresh (a)
                  (caro l a)
                  (conso a res out))))))
;; 3 freshes because res is in both subfreshes
;; the two subfreshes are independent

;; The Translation (Final):
;; To translate function to relation
;;   1) Replace define with defrel
;;   2) Unnest each expression in each cond line
;;   3) Replace each cond with conde
;;   4) To unnest #t replace with succeed
;;   5) To unnest #f replace with fail
;;   6) If the value of a cond line is non-Boolean
;;      add an argument to defrel to hold what was
;;      the function value.
;;   7) When unnesting a line whose value is not
;;      a Boolean, ensure that either some value is
;;      associated with out or out is the last arg
;;      to a recursion.

;; we can merge the freshes
;; use cons instead of car/cdr
(defrel (appendo-second l t out)
  (conde
   ((nullo l) (== t out))
   ((fresh (a d res)
           (conso a d l)
           (appendo-second d t res)
           (conso a res out)))))

;; Simplifiy loso, lolo, proper-membero
(defrel (lolo-simple l)
  (conde
   ((nullo l))
   ((fresh (a d)
           (conso a d l)
           (listo a)
           (lolo-simple d)))))

(defrel (loso-simple l)
  (conde
   ((nullo l))
   ((fresh (a d)
           (conso a d l)
           (singletono a)
           (loso-simple d)))))

(defrel (proper-membero-simple x l)
  (fresh (a d)
         (conso a d l)
         (conde
          ((== a x) (listo d))
          ((proper-membero-simple x d)))))

;; swap the last 2 goals of appendo
(defrel (appendo l t out)
  (conde
   ((nullo l) (== t out))
   ((fresh (a d res)
           (conso a d l)
           (conso a res out)
           (appendo d t res)))))

;; First Commandment: Within each sequence of goals, move non-recursive goals
;;                    before recursive goals.

(defrel (swappendo l t out)
  (conde
   ((fresh (a d res)
           (conso a d l)
           (conso a res out)
           (swappendo d t res)))
   ((nullo l) (== t out))))

;; Law of Swapping conde Lines: Swapping 2 conde lines does not affect the
;;                              values contributed by conde

;; unwrap: Unwraps pairs until a value occurs
;; if x is not a pair, return x (identity function)
(define (unwrap x)
  (cond
    ((pair? x) (unwrap (car x)))
    (#t x)))

(defrel (unwrapo x out)
  (conde
   ((fresh (a)
           (caro x a)
           (unwrapo a out)))
   ((== x out))))

;; CHAPTER 5: Members Only

;; mem (Atom,List) -> #f | List
;; like member? but returns rest of list rather than true
(define (mem x l)
  (cond
    ((null? l) #f)
    ((equal? (car l) x) l)
    (#t (mem x (cdr l)))))

(defrel (memo-first x l out)
  (conde
   ((nullo l) fail)
   ((fresh (a)
           (caro l a)
           (== a x))
    (== l out))
   (succeed (fresh (d)
                   (cdro l d)
                   (memo-first x d out)))))

(defrel (memo x l out)
  (conde
   ((caro l x) (== l out))
   ((fresh (d)
           (cdro l d)
           (memo x d out)))))

;; rember: (Atom, List) -> List
;; Removes an atom once if it exists
(define (rember x l)
  (cond
    ((null? l) '())
    ((equal? (car l) x) (cdr l))
    (#t (cons (car l)
              (rember x (cdr l))))))

(defrel (rembero-first x l out)
  (conde
   ((nullo l) (== '() out))
   ((fresh (a)
           (caro l a)
           (== a x))
    (cdro l out))
   (succeed
    (fresh (d)
           (cdro l d)
           (rembero-first x d res))
    (fresh (a)
           (caro l a)
           (conso a res out)))))

(defrel (rembero x l out)
  (conde
   ((nullo l) (== '() out))
   ((conso x out l))
   ((fresh (a d res)
           (conso a d l)
           (conso a res out)
           (rembero x d res)))))

;; CHAPTER 6: The Fun Never Ends...

(defrel (alwayso)
  (conde
   (succeed)
   ((alwayso))))

(defrel (nevero)
  (nevero))

(defrel (very-recursiveo)
  (conde
   ((nevero))
   ((very-recursiveo))
   ((alwayso))
   ((very-recursiveo))
   ((nevero))))

;; CHAPTER 7: A Bit Too Much

(defrel (bit-xoro x y r)
  (conde
   ((== 0 x) (== 0 y) (== 0 r))
   ((== 0 x) (== 1 y) (== 1 r))
   ((== 1 x) (== 0 y) (== 1 r))
   ((== 1 x) (== 1 y) (== 0 r))))

(defrel (bit-ando x y r)
  (conde
   ((== 0 x) (== 0 y) (== 0 r))
   ((== 0 x) (== 1 y) (== 0 r))
   ((== 1 x) (== 0 y) (== 0 r))
   ((== 1 x) (== 1 y) (== 1 r))))

(defrel (half-addero x y r c)
  (bit-xoro x y r)
  (bit-ando x y c))

(defrel (full-addero b x y r c)
  (fresh (w xy wz)
         (half-addero x y w xy)
         (half-addero w b r wz)
         (bit-xoro xy wz c)))

(define (build-num-first n)
  (cond
    ((zero? n) '())
    ((even? n) (cons 0 (build-num-first (/ n 2))))
    ((odd? n) (cons 1 (build-num-first (/ (- n 1) 2))))))

;; redefine build-num so that each cond line is 'non-overlapping'.
;; This property is necessary for intended behavior when converted
;; into a relation.
(define (build-num n)
  (cond
    ((odd? n) (cons 1 (build-num (/ (- n 1) 2))))
    ((and (not (zero? n)) (even? n)) (cons 0 (build-num (/ n 2))))
    ((zero? n) '())))

(defrel (poso n)
  (fresh (a d)
         (== `(,a . ,d) n)))

(defrel (>1o n)
  (fresh (a ad dd)
         (== `(,a ,ad . ,dd) n)))

(defrel (addero b n m r)
  (conde
   ((== 0 b) (== '() m) (== n r))
   ((== 0 b) (== '() n) (== m r) (poso m))
   ((== 1 b) (== '() m) (addero 0 n '(1) r))
   ((== 1 b) (== '() n) (poso m) (addero 0 '(1) m r))
   ((== '(1) n) (== '(1) m)
               (fresh (a c)
                      (== `(,a ,c) r)
                      (full-addero b 1 1 a c)))
   ((== '(1) n) (gen-addero b n m r))
   ((== '(1) m) (>1o n) (>1o r) (addero b '(1) n r))
   ((>1o n) (gen-addero b n m r))))

(defrel (gen-addero b n m r)
  (fresh (a c d e x y z)
         (== `(,a . ,x) n)
         (== `(,d . ,y) m) (poso y)
         (== `(,c . ,z) r) (poso z)
         (full-addero b a d c e)
         (addero e x y z)))

(defrel (+o n m k)
  (addero 0 n m k))

(defrel (-o n m k)
  (+o m k n))

(define (length-my l)
  (cond
    ((null? l) 0)
    (#t (+ 1 (length (cdr l))))))

(defrel (lengtho l n)
  (conde
   ((nullo l) (== '() n))
   ((fresh (d res)
           (cdro l d)
           (+o '(1) res n)
           (lengtho d res)))))

;; CHAPTER 8: Just a Bit More
